#include "wordwidget.h"
#include "ui_wordwidget.h"

WordWidget::WordWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WordWidget)
{
    ui->setupUi(this);
}

WordWidget::~WordWidget()
{
    delete ui;
}

void WordWidget::displayWord(Word *wrd, bool hide)
{
    QString pic = getPictureForWord(wrd, hide);
    QPixmap pix = QPixmap::fromImage(QImage(pic));

    QPainter paint(&pix);
    if(!hide)//maître espion
    {
        if(wrd->getTeam() == Team::Witness)
            paint.setPen(QColor(0,0,0));
        else
            paint.setPen(QColor(255,255,255));
    }
    else//agent
    {
        paint.setPen(QColor(0,0,0));
    }
    paint.setFont(this->font());
    paint.drawText(pix.rect(), Qt::AlignCenter, wrd->value);
    this->ui->label->setPixmap(pix);
}

QString WordWidget::getPictureForWord(Word *wrd, bool hide)
{
    QString path = ":/images/images/";
    if(wrd->isDiscovered()){
        //Mot découvert
        if(wrd->getTeam() == Team::Red){
            path+="inform_red.png";
        } else if(wrd->getTeam() == Team::Blue) {
            path+="inform_blue.png";
        } else if(wrd->getTeam() == Team::Witness){
            path+="witness.png";
        } else if(wrd->getTeam() == Team::Assassin){
            path+="assassin.png";
        }
    } else {
        if(!hide){ //Maitre-espion
            if(wrd->getTeam() == Team::Red){
                path+="word_red.png";
            } else if(wrd->getTeam() == Team::Blue) {
                path+="word_blue.png";
            } else if(wrd->getTeam() == Team::Witness){
                path+="blank.png";
            } else if(wrd->getTeam() == Team::Assassin){
                path+="word_assassin.png";
            }
        } else {//Agent
            path+="blank.png";
        }
    }
    return path;
}

void WordWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    emit clicked(number);
    QWidget::mouseReleaseEvent(ev);
}
