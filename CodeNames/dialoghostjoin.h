#ifndef DIALOGHOSTJOIN_H
#define DIALOGHOSTJOIN_H

#include <QDialog>

#include "globaldata.h"
#include "network.h"
#include "servernetwork.h"
#include "clientnetwork.h"
#include "setupwindow.h"
#include "lobbywindow.h"

namespace Ui {
class DialogHostJoin;
}

class DialogHostJoin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogHostJoin(QWidget *parent = nullptr);
    ~DialogHostJoin();

signals:
    void windowClosingCancel();
    void windowClosingContinue();

private slots:
    void on_btnHost_clicked();

    void on_btnJoin_clicked();

private:
    Ui::DialogHostJoin *ui;
    void closeEvent(QCloseEvent*ev);
    bool continuing = false;
};

#endif // DIALOGHOSTJOIN_H
