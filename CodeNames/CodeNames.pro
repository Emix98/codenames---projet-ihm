#-------------------------------------------------
#
# Project created by QtCreator 2018-10-17T14:13:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CodeNames
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        codenameswindow.cpp \
    network.cpp \
    clientnetwork.cpp \
    servernetwork.cpp \
    fakenetwork.cpp \
    chatwindow.cpp \
    launcherwindow.cpp \
    word.cpp \
    dialogaide.cpp \
    dialoghostjoin.cpp \
    dialogpause.cpp \
    lobbywindow.cpp \
    dialogoptions.cpp \
    globaldata.cpp \
    setupwindow.cpp \
    gword.cpp \
    player.cpp \
    gplayer.cpp \
    gameengine.cpp \
    playerlobbywidget.cpp \
    gamestatus.cpp \
    wordwidget.cpp

HEADERS  += codenameswindow.h \
    network.h \
    clientnetwork.h \
    servernetwork.h \
    networkmessages.h \
    networkerrors.h \
    fakenetwork.h \
    chatwindow.h \
    icontentwidget.h \
    launcherwindow.h \
    word.h \
    team.h \
    dialogaide.h \
    dialoghostjoin.h \
    dialogpause.h \
    lobbywindow.h \
    dialogoptions.h \
    globaldata.h \
    setupwindow.h \
    gword.h \
    player.h \
    gplayer.h \
    gameengine.h \
    playerlobbywidget.h \
    gamestatus.h \
    wordwidget.h \
    mod.h

FORMS    += codenameswindow.ui \
    chatwindow.ui \
    launcherwindow.ui \
    dialogaide.ui \
    dialoghostjoin.ui \
    dialogpause.ui \
    lobbywindow.ui \
    dialogoptions.ui \
    setupwindow.ui \
    playerlobbywidget.ui \
    wordwidget.ui

RESOURCES += \
    resouurces.qrc

TRANSLATIONS = CodeNames_en.ts \
	CodeNames_fr.ts
