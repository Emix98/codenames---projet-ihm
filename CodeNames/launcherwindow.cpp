#include "launcherwindow.h"
#include "ui_launcherwindow.h"



LauncherWindow::LauncherWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LauncherWindow)
{
    ui->setupUi(this);
}

LauncherWindow::~LauncherWindow()
{
    delete ui;
}

void LauncherWindow::retranslateUI()
{
    this->ui->retranslateUi(this);
}


void LauncherWindow::on_local_button_clicked()
{
    //On ouvre la fenètre de partie locale
    SetupWindow* win = new SetupWindow(this);
    this->setVisible(false);
    win->show();

    connect(win, SIGNAL(windowClosing()), this, SLOT(window_closed()));
}

void LauncherWindow::on_options_button_clicked()
{
    DialogOptions options(this);
    options.setModal(true);
    options.exec();
}

void LauncherWindow::on_help_button_clicked()
{
    //On affiche le dialogue d'aide, qui sera supprimé à sa fermeture
    DialogAide* aide = new DialogAide(this);
    aide->show();
    aide->setModal(false);
    aide->setAttribute(Qt::WA_DeleteOnClose);
}

void LauncherWindow::on_quit_button_clicked()
{
    //on se contente de quitter
    qApp->exit();
}

void LauncherWindow::window_closed()
{
    QObject* otherWin = sender();

    QWidget* win = qobject_cast<QWidget*>(otherWin);

    if(win != nullptr){
        //On re-affiche cette fenètre
        this->setVisible(true);
        //Puis on supprime celle qui vient de se fermer
        win->deleteLater();
    }
}

void LauncherWindow::on_network_button_clicked()
{
    //On crée le dialogue
    DialogHostJoin* dhj = new DialogHostJoin(this);
    //On rend cette fenètre invisible
    this->setVisible(false);
    //On affiche le dialogue
    dhj->show();
    //Et on connecte le signal nécessaire pour arréter
    connect(dhj, SIGNAL(windowClosingCancel()), this, SLOT(window_closed()) );
}

void LauncherWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        // retranslate designer form
        ui->retranslateUi(this);
    }

    // remember to call base class implementation
    QWidget::changeEvent(event);
}
