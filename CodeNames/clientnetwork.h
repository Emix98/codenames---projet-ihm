#ifndef CLIENTNETWORK_H
#define CLIENTNETWORK_H

#include <QtNetwork/QTcpSocket>
#include <QByteArray>
#include <QDataStream>

#include "network.h"
/**
 * @brief Implémentation de Network pour un client réseau
 * Se crée avec une instanciation et se démarre à l'appel de la méthode start
 * S'arrète avec la méthode quit
 */
class ClientNetwork : public Network
{
    //Permet le support des signaux/slots
    Q_OBJECT

public:
    /**
     * @brief ClientNetwork : Crée une nouvelle instance client pour connection au serveur à l'addresse adress et au port port
     * @param address : l'addresse du serveur sur lequel se connecter
     * @param port : port auquel se connecter
     */
    ClientNetwork(QString address, int port);
    /**
     * @brief getPort
     * @return Le port de connection à la partie si applicable, 0 sinon
     */
    virtual int getPort();
    /**
     * @brief getAdresses : retourne l'addresse du serveur sur lequel se connecter
     * @return une liste contenant uniquement l'addresse du serveur qui doit être libérée par la suite (via delete)
     */
    virtual QList<QString>* getAdresses();
    /**
     * @brief isReady : indique si la connectique à démarrer
     * @return si le client est prêt
     */
    virtual bool isReady();
    /**
     * Le déstructeur pour le nettoyage
     */
    ~ClientNetwork();

public slots:
    /**
     * @brief sendChatMessage : implémentation du solt définit dans Network
     * @param msg : le message de chat à transférer
     */
    virtual void sendChatMessage(QString msg);
    /**
     * @brief sendData : envoie des données pour un message spécifié au serveur
     * @param msg : le type de message (ex CHAT_MESSAGE pour le chat)
     * @param data : les données associées au message
     */
    void sendData(NetworkMessage msg, QByteArray data);

    /**
     * @brief sendTeamChatMessage : permet d'envoyer un message de chat à une équipe
     * @param msg : le message à transmettre
     * @param tea : l'équipe de destination du message
     */
    virtual void sendTeamChatMessage(QString msg, Team tea);

private:
    /**
     * @brief address : l'addresse du serveur
     */
    QString address;
    /**
     * @brief port : le port du serveur
     */
    int port;
    /**
     * @brief socket : la socket qui correspont à la connection avec le serveur
     */
    QTcpSocket * socket;
    /**
     * @brief ready : si la classe est prête
     */
    bool ready = false;
    /**
     * @brief run : méthode lancée au démarrage du thread
     */
    virtual void run();


private slots:
    /**
     * @brief onDataReceived : slot appelée à chaque fois qu'on reçoit des données du serveur
     */
    void onDataReceived();
    /**
     * @brief onConnection : appelé une fois que la connection est établie
     */
    void onConnection();
    /**
     * @brief onDisconnection : appelé quand la connection est fermée
     */
    void onDisconnection();
    /**
     * @brief onSocketError : appelé quand une erreur survient sur la socket
     * @param error : l'erreur
     */
    void onSocketError(QAbstractSocket::SocketError error);
};

#endif // CLIENTNETWORK_H
