#ifndef GPLAYER_H
#define GPLAYER_H
#include <QList>
#include <QString>
#include "player.h"

class GPlayer
{
public:
    /**
     * @brief getPlayerById : va chercher un joueur dans la liste des joueurs à partir d'un identifiant
     * @param id : id du joueur à chercher
     * @return  : un pointeur vers le joueur, un pointeur null sinon
     */
    static Player* getPlayerById(int id);


    /**
     * @brief getPlayerByName : va chercher un joueur dans la liste des joueurs à partir d'un nom
     * @param name : le nom du joueur à chercher
     * @return : une liste de joueur (plusieurs joueurs peuvent avoir le même nom)
     */
    static QList<Player*> getPlayerByName(QString name);


    /**
     * @brief getPlayers : donne la liste des joueurs
     * @return : liste des joueurs de la partie
     */
    static QList<Player*> getPlayers();


    /**
     * @brief getBlues : donne la liste des joueurs bleus
     * @return liste de joueurs bleus
     */
    static QList<Player*> getBlues();


    /**
     * @brief getReds : donne la liste des joueurs rouges
     * @return : liste de joueurs rouges
     */
    static QList<Player*> getReds();

    /**
     * @brief createPlayer : créé un joueur
     * @param n : nom du joueur
     * @param t : équipe dans laquelle il doit se trouver
     * @param pT : type du joueur (agent ou maître-espion)
     * @return : le joueur créé
     */
    static Player* createPlayer(QString n, Team t, PType pT);
    /**
     * @brief deletePlayer : retire un joueur de la liste puis le détruit
     * @param id : id du joueur à supprimer
     * @return : 0 si la suppression s'est bien passé, -1 sinon
     */
    static int deletePlayer(int id);

private:
    static QList <Player*> playerList;
    };

#endif // GPLAYER_H
