#ifndef PLAYERLOBBYWIDGET_H
#define PLAYERLOBBYWIDGET_H

#include <QWidget>

#include "player.h"
#include "team.h"

namespace Ui {
class PlayerLobbyWidget;
}

class PlayerLobbyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerLobbyWidget(QWidget *parent = 0, bool host = false);
    ~PlayerLobbyWidget();

    bool ignoreNextPlayerUpdate();

    Player getPlayer();
    void setKickButtonEnabled(bool state);
    void setPlayer(Player &plr);
signals:
    void playerChanged();

private:
    Ui::PlayerLobbyWidget *ui;
    bool ignoreNext = false;
};

#endif // PLAYERLOBBYWIDGET_H
