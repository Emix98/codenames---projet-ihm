<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ChatWindow</name>
    <message>
        <location filename="chatwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="36"/>
        <source>Global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="52"/>
        <location filename="chatwindow.ui" line="76"/>
        <location filename="chatwindow.ui" line="100"/>
        <source>Envoyer</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="60"/>
        <source>Equipe</source>
        <translation>Team</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="84"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
</context>
<context>
    <name>CodeNamesWindow</name>
    <message>
        <location filename="codenameswindow.ui" line="14"/>
        <source>CodeNamesWindow</source>
        <translation>CodeNamesWindow</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Equipe&lt;br/&gt;bleue&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Blue&lt;br/&gt;team&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="148"/>
        <source>Espions Restants :</source>
        <translation>remaining spy :</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="171"/>
        <location filename="codenameswindow.cpp" line="87"/>
        <source>Valider</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="197"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="220"/>
        <location filename="codenameswindow.ui" line="318"/>
        <source>Nbespionb</source>
        <translation>NbSpyb</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="240"/>
        <source>étape en cours</source>
        <translation>Current step</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="275"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Param</source>
        <translation type="vanished">Param</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="295"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Equipe&lt;br/&gt;Rouge&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Red&lt;br/&gt;team&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="338"/>
        <source>Indice :</source>
        <translation>Clue :</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="354"/>
        <source>Symbole d&apos;équipe actuelle</source>
        <translation>Current team logo</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="59"/>
        <location filename="codenameswindow.cpp" line="61"/>
        <location filename="codenameswindow.cpp" line="68"/>
        <location filename="codenameswindow.cpp" line="70"/>
        <source>Fin de partie</source>
        <translation>Game Over
</translation>
    </message>
    <message>
        <source>L&apos;équipe rouge a perdu...</source>
        <translation type="vanished">Red team as lost...</translation>
    </message>
    <message>
        <source>L&apos;équipe bleue a perdu...</source>
        <translation type="vanished">The blue team has lost...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="59"/>
        <source>L&apos;équipe rouge a trouvé l&apos;assassin, elle a perdu...</source>
        <translation>The red team found the assassin, they lost...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="61"/>
        <source>L&apos;équipe bleue a trouvé l&apos;assassin, elle a perdu...</source>
        <translation>The blue team found the assassin, they lost...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="68"/>
        <source>L&apos;équipe rouge a gagné !</source>
        <translation>Red Team Won !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="70"/>
        <source>L&apos;équipe bleue a gagné !</source>
        <translation>Red Team Won !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="74"/>
        <location filename="codenameswindow.cpp" line="160"/>
        <location filename="codenameswindow.cpp" line="179"/>
        <source>Attention</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="74"/>
        <source>Tour d&apos;un maître espion, vérifiez qu&apos;aucun agent ne voit l&apos;écran !</source>
        <translation>Turn of the SpyMaster, please check no agent see the screen !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="79"/>
        <source>Fin de tour</source>
        <translation>End of the turn</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="104"/>
        <source>Maitre Espion</source>
        <translation>SpyMaster</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="105"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="106"/>
        <source>Rouge</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="107"/>
        <source>Bleu</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="160"/>
        <source>Le chat n&apos;est pas disponible en partie locale</source>
        <translation>The Chat is not available in a local game</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="179"/>
        <source>Indice vide !</source>
        <translation>Hint empty !</translation>
    </message>
</context>
<context>
    <name>DialogAide</name>
    <message>
        <location filename="dialogaide.ui" line="14"/>
        <source>CodeNames: Aide</source>
        <translation>CodeNames: Help</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="27"/>
        <source>Agents</source>
        <translation>Agents</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="32"/>
        <source>Maître-espion</source>
        <translation>SpyMaster</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="37"/>
        <source>Jeu</source>
        <translation>Game</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="42"/>
        <source>Tour</source>
        <translation>Turn</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="56"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Cliquez sur l&apos;une des options dans la liste pour en apprendre plus sur les différents aspects de CodeNames !&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Clilc on one of the options on the list to know more about CodeNames !&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="89"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
</context>
<context>
    <name>DialogHostJoin</name>
    <message>
        <location filename="dialoghostjoin.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="39"/>
        <location filename="dialoghostjoin.ui" line="148"/>
        <source>Héberger</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="45"/>
        <source>Nom de la partie :</source>
        <translation>Name of the game :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="55"/>
        <source>Entrez un nom</source>
        <translation>Enter a name</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="75"/>
        <source>Nombre de joueurs :</source>
        <translation>Number of Players :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="102"/>
        <source>Port d&apos;écout e:</source>
        <translation>Listening port :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="169"/>
        <location filename="dialoghostjoin.ui" line="221"/>
        <source>Rejoindre</source>
        <translation>Join</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="175"/>
        <source>Adresse de l&apos;hôte</source>
        <translation>Host adress</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="198"/>
        <source>Port de connexion</source>
        <translation>Connection port</translation>
    </message>
</context>
<context>
    <name>DialogOptions</name>
    <message>
        <location filename="dialogoptions.ui" line="14"/>
        <source>CodeNames: Options</source>
        <translation>CodeNames: Options</translation>
    </message>
    <message>
        <location filename="dialogoptions.ui" line="22"/>
        <source>Langue :</source>
        <translation>Language :</translation>
    </message>
    <message>
        <source>Français</source>
        <translation type="vanished">French</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">English</translation>
    </message>
    <message>
        <location filename="dialogoptions.ui" line="45"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>DialogPause</name>
    <message>
        <location filename="dialogpause.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="64"/>
        <source>Jeu en pause</source>
        <translation>Game paused</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="79"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="91"/>
        <source>Reprendre</source>
        <translation>Resume</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="103"/>
        <source>Recommencer</source>
        <translation>Start again</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="115"/>
        <source>Quitter</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>LauncherWindow</name>
    <message>
        <location filename="launcherwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ceci est une image&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is a picture&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="45"/>
        <source>Partie locale</source>
        <translation>Local game</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="55"/>
        <source>Partie en réseau</source>
        <translation>Network game</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="62"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="69"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="76"/>
        <source>Quitter</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>LobbyWindow</name>
    <message>
        <location filename="lobbywindow.ui" line="14"/>
        <source>CodeName : Lobby</source>
        <translation>CodeName : Lobby</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="24"/>
        <source>Démarrer</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="44"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="60"/>
        <source>Envoyer</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="70"/>
        <source>Addresse(s) du serveur :</source>
        <translation>Server adress :</translation>
    </message>
</context>
<context>
    <name>PlayerLobbyWidget</name>
    <message>
        <location filename="playerlobbywidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="20"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="31"/>
        <source>Maitre</source>
        <translation>SpyMaster</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="36"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="45"/>
        <source>Rouge</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="50"/>
        <source>Bleue</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>SetupWindow</name>
    <message>
        <location filename="setupwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="63"/>
        <source>Nombre d&apos;espions rouge</source>
        <translation>Number of red spys</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="109"/>
        <source>Nombre d&apos;espions bleu</source>
        <translation>Number of blue spys</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="152"/>
        <source>Nombre d&apos;assassin</source>
        <translation>Number of assassins</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="178"/>
        <source>Aléatoire</source>
        <translation>Random</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="183"/>
        <source>Rouge</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="188"/>
        <source>Bleu</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="206"/>
        <source>Équipe de départ</source>
        <translation>Team that starts</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="252"/>
        <source>Témoins</source>
        <translation>Witness</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="270"/>
        <source>Ne jouez qu&apos;une seule équipe (2 joueurs minimum)</source>
        <translation>Only one team play (at least 2 player required)</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="273"/>
        <source>Coopératif</source>
        <translation>Cooperative</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="291"/>
        <source>Afrontez vous (4 joueurs minimum)</source>
        <translation>Compete between you and your friend (at least 4 players needed)</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="294"/>
        <source>Compétitif</source>
        <translation>Competitive</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="304"/>
        <source>Commencer</source>
        <translation>Start Game</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="316"/>
        <source>Choisissez le mode de jeu :</source>
        <translation>Choose the game mode :</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="333"/>
        <source>Personnalisation</source>
        <translation>Tweak</translation>
    </message>
</context>
<context>
    <name>WordWidget</name>
    <message>
        <location filename="wordwidget.ui" line="20"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
</TS>
