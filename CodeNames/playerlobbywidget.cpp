#include "playerlobbywidget.h"
#include "ui_playerlobbywidget.h"

PlayerLobbyWidget::PlayerLobbyWidget(QWidget *parent, bool host) :
    QWidget(parent),
    ui(new Ui::PlayerLobbyWidget)
{
    ui->setupUi(this);
    //Si c'est pas l'hôte on vire le contenu du label
    if(!host)
        ui->lblHost->setText("");

    //On connecte le changement d'un attribut au signal d'information que le joueur à changer
    connect(this->ui->cboRole, SIGNAL(currentIndexChanged(int)), this, SIGNAL(playerChanged()));
    connect(this->ui->cboTeam, SIGNAL(currentIndexChanged(int)), this, SIGNAL(playerChanged()));
    connect(this->ui->lePlrName, SIGNAL(textEdited(QString)), this, SIGNAL(playerChanged()));

}

Player PlayerLobbyWidget::getPlayer(){
    Player plr(this->ui->lePlrName->text(),
               this->ui->cboRole->currentIndex() == 0 ? Team::Red : Team::Blue,//L'index 0 est celui de l'équipe rouge
               this->ui->cboRole->currentIndex() == 0 ? PType::SpyMaster : PType::Agent);//L'index 0 est celui du maitre
    return plr;
}

void PlayerLobbyWidget::setPlayer(Player& plr){
    this->ignoreNext = true;
    this->ui->lePlrName->setText(plr.name);
    this->ui->cboRole->setCurrentIndex(plr.pType == PType::SpyMaster ? 0 : 1);
    this->ui->cboTeam->setCurrentIndex(plr.team == Team::Red ? 0 : 1);
}

PlayerLobbyWidget::~PlayerLobbyWidget()
{
    delete ui;
}

bool PlayerLobbyWidget::ignoreNextPlayerUpdate()
{
    bool temp = this->ignoreNext;
    this->ignoreNext = false;
    return temp;
}

void PlayerLobbyWidget::setKickButtonEnabled(bool state){
    this->ui->kickBtn->setEnabled(state);
}
