#ifndef GAMESTATUS_H
#define GAMESTATUS_H

#include <QObject>
#include <QString>
#include <QList>

#include "word.h"
#include "team.h"
#include "player.h"
#include "mod.h"

class GameStatus : public QObject
{
    Q_OBJECT

public:
    Mod mod;
    int blueInformant;
    int redInformant;
    Team teamTurn;
    PType roleTurn;
    QString currentClue;
    int nbWordsForClue;

    GameStatus(QObject *parent, Mod mode, int blueInf, int redInf, Team curTeamTurn, PType curRoleTurn, QString curClue, int nbWForClue);

    QByteArray serialize();
    void updateViaString(QByteArray byteStatus);
    static GameStatus* unserialize(QByteArray byteStatus);

    void decrInf(Team t);

    int getInf(Team t);

    void markUpdated();

signals:
    void updated();

public slots:
};

#endif // GAMESTATUS_H
