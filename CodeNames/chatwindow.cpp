#include "chatwindow.h"
#include "ui_chatwindow.h"

ChatWindow::ChatWindow(QWidget *parent, Player* plr, Network *net) :
    QDialog(parent),
    ui(new Ui::ChatWindow), localPlayer(plr), network(net)
{
    ui->setupUi(this);
    //Connection pour l'envoie des messages
    connect(this, SIGNAL(newChatMessage(QString)), this->network, SLOT(sendChatMessage(QString)));
    //Connection pour la reception des messages
    connect(this->network, SIGNAL(onChatMessageReceived(QString)), this, SLOT(receiveChatMessage(QString)));

    //On connect les boutons
    connect(this->ui->sendBtnAgent, SIGNAL(clicked()), this, SLOT(on_sendBtn_clicked()));
    connect(this->ui->sendBtnTeam, SIGNAL(clicked()), this, SLOT(on_sendBtn_clicked()));
    connect(this->ui->sendBtnGlobal, SIGNAL(clicked()), this, SLOT(on_sendBtn_clicked()));

    //On désactive cet onglet, on ne l'utilise finalement pas
    this->ui->tabAgent->setEnabled(false);
}

ChatWindow::~ChatWindow()
{
    delete ui;
    //déconnection de l'envoie des messages
    disconnect(this, SIGNAL(newChatMessage(QString)), this->network, SLOT(sendChatMessage(QString)));
    //déconnection de la reception des messages
    disconnect(this->network, SIGNAL(onChatMessageReceived(QString)), this, SLOT(receiveChatMessage(QString)));
}

void ChatWindow::receiveChatMessage(QString message)
{
    //Appelé quand on reçoit un message
    //On ajoute le message à la fin de la zone de texte
    this->ui->chatDisplayGlobal->append(message);
    //Puis on met la zone en bas
    QScrollBar *sb = this->ui->chatDisplayGlobal->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void ChatWindow::receiveChatMessageTeam(QString message, Team tea)
{
    //Quand on reçoit un message d'équipe de la bonne équipe
    if(tea == this->localPlayer->team){
        //On le rajoute à la liste
        this->ui->chatDisplayTeam->append(message);

        //Et on met la zone en bas
        QScrollBar *sb = this->ui->chatDisplayTeam->verticalScrollBar();
        sb->setValue(sb->maximum());
    }
}

void ChatWindow::on_sendBtn_clicked()
{
    //On récupère le bouton d'envoi
    QPushButton* sigSrc = qobject_cast<QPushButton*>(sender());


    if(sigSrc == this->ui->sendBtnGlobal){
        //Quand le bouton d'envoi est clické (ou quand Entrée est pressé sur la ligne de texte)
        //On récupère le message à envoyer
        QString message = this->ui->inputLineGlobal->text();
        //On envoie le signal
        emit newChatMessage(this->localPlayer->name+ " : " + message);

        //On retire le texte du champ de texte :
        this->ui->inputLineGlobal->setText("");
        //On affiche pas le message directement, on attend la réponse du serveur/fakenetwork
        //Celà permet de simplifier une partie du code de gestion du réseau côté serveur (voir ServerNetwork::onDataReceived)
    } else if(sigSrc == this->ui->sendBtnAgent){
        //TODO: implémenter différents chat quand player est prèt
    } else if(sigSrc == this->ui->sendBtnTeam){
        QString message = this->ui->inputLineTeam->text();
        //On envoie le signal
        emit newChatMessageTeam(message, this->localPlayer->team);
    }


}
