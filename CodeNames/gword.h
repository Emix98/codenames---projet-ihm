#ifndef GWORD_H
#define GWORD_H

#include <QString>
#include <algorithm>
#include <QFile>
#include <QTextStream>
#include <QList>
#include <QRandomGenerator>
#include "word.h"



class GWord
{
public:
    /**
     * @brief initWords : Initialise le tableau de 25 mots
     * @param filename : chemin du fichier contenant les mots
     * @param nbBlue : nombre de mots bleus
     * @param nbRed : nombre de mots rouges
     * @param nbAss : nombre de mots assassins
     * @return  : faux si la lecture du fichier échoue, vrai sinon
     */
    static bool initWords(QString filename, int nbBlue, int nbRed, int nbAss);

    /**
     * @brief cleanWords : Nettoie le tableau de mots
     */
    static void cleanWords();

    /**
     * @brief setWords : Transforme le tableau de mots pour qu'il corresponde au tableau fourni
     * @param wds : tableau à utiliser
     */
    static void setWords(Word *wds);

    /**
     * @brief getWord : Renvoie un pointeur sur le mot à l'indice donné
     * @param id : indice du mot
     * @return pointeur sur le mot
     */
    static Word* getWord(int id);

    /**
     * @brief discoverWord : appelle la fonction discover du mot à l'indice donné
     * @param id : indice du mot
     * @return équipe du mot découvert
     */
    static Team discoverWord(int id);

    /**
     * @brief discoverRed : découvre un mot rouge (pour le mode coop)
     */
    static void discoverRed();

private:
    static Word words[25];

};

#endif // GWORD_H
