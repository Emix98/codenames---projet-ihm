#ifndef PLAYER_H
#define PLAYER_H
#include <QString>
#include <team.h>

enum PType {
    Agent,
    SpyMaster,
    Murdered
};

class Player
{
public:
    /**
     * @brief cptPlayer : compteur d'instance de Player
     */
    static int cptPlayer;
    QString const name;
    PType pType;
    Team team;
    /**
     * @brief id : identififiant unique pour chaque player (basé sur le compteur d'instance)
     */
    int id;

    /**
     * @brief Player : constructeur par défaut
     */
    Player();

    /**
     * @brief Player : constructeur de copie
     * @param other : l'objet player à copier
     */
    Player(const Player& other);


    /**
     * @brief Player : constructeur
     * @param n : nom du joueur
     * @param t : équipe auquel il appartient
     * @param pT : type du joueur (agent ou maitre-espion)
     */
    Player(QString n, Team t, PType pT);
};

#endif // PLAYER_H
