#include "dialogoptions.h"
#include "ui_dialogoptions.h"


DialogOptions::DialogOptions(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOptions)
{
    ui->setupUi(this);
    this->ui->cboLang->setCurrentText(GlobalData::options["lang"]);
}

DialogOptions::~DialogOptions()
{
    delete ui;
}

void DialogOptions::on_cboLang_currentTextChanged(const QString &arg1)
{
    GlobalData::options["lang"] = arg1;

    //On change la locale et on récupère le fichier approprié
    QLocale lcle;
    QString langFile;
    QString oldFile;
    if(arg1 == QString("English")){
        langFile = "CodeNames_en.qm";
        oldFile  = "CodeNames_fr.qm";
        lcle = QLocale(QLocale::English);
    } else {
        langFile = "CodeNames_fr.qm";
        oldFile  = "CodeNames_en.qm";
        lcle = QLocale(QLocale::French);
    }

    QString path = ":/traductions";

    QTranslator translator;
    QTranslator oldTranslator;
    //On charge les traducteurs
    translator.load(path +"/"+langFile);
    oldTranslator.load(path +"/"+oldFile);


    // remove the old translator
    QApplication::removeTranslator(&oldTranslator);

    // load the new translator
    QApplication::installTranslator(&translator);

    //et on rafraichi les traductions affichées
    this->ui->retranslateUi(this);

    LauncherWindow* lchWin = qobject_cast<LauncherWindow*>(this->parent());

    if(lchWin != nullptr){
        lchWin->setLocale(lcle);
        lchWin->retranslateUI();
    }

}
