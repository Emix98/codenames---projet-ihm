#ifndef NETWORKMESSAGES_H
#define NETWORKMESSAGES_H

#include <QtGlobal>

enum NetworkMessage : unsigned char {
    // Le message initial contenant les informations nécessaires
    INIT_CONNECTION = 'a',

    // Messages pour le chat
    CHAT_MESSAGE,
    CHAT_MESSAGE_TEAM,

    // Messages pour la communication des joueurs
    PLAYER_CONNECTED,
    PLAYER_NAME_CHANGE,
    PLAYER_DISCONNECTED,
    PLAYER_LIST,

    // Messages pour le GameStatus
    GAMESTATUS_INITIALISATION,
    GAMESTATUS_UPDATE,

    // Messages action
    WORD_REVEAL_VOTE,
    WORD_REVEAL,

    // Messages d'erreurs
    ENGINE_ERROR

};


#endif // NETWORKMESSAGES_H
