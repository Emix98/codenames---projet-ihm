#ifndef GLOBALDATA_H
#define GLOBALDATA_H

#include <QMap>
#include <QString>

#include "network.h"
#include "gamestatus.h"

class GlobalData
{

public:
    static QMap<QString, QString> options;
    static Network* network;
    static GameStatus* status;
    static void initOptions();

private:
    GlobalData();

};

#endif // GLOBALDATA_H
