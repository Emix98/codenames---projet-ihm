#include "gameengine.h"

QString GameEngine::indice;

void GameEngine::discover(int tuile)
{
    if(GlobalData::status->roleTurn == PType::SpyMaster)
        return;

    Team t = GWord::discoverWord(tuile);
    if(t == Team::Assassin)
    {
        GlobalData::status->roleTurn = PType::Murdered;
        GlobalData::status->markUpdated();
        return;
    }
    else if(t == Team::Witness)
        endOfTurn();
    else
    {
        GlobalData::status->decrInf(t);
        if(GlobalData::status->getInf(t) == 0)
        {
            endOfGame();
            return;
        }
        if(GlobalData::status->teamTurn  != t)
        {
            endOfTurn();
            return;
        }
        GlobalData::status->markUpdated();
    }
}

void GameEngine::endOfTurn()
{
    switch(GlobalData::status->mod)
    {
        case Mod::Compet:
        if(GlobalData::status->roleTurn == PType::Agent)
        {
            if(GlobalData::status->teamTurn == Team::Blue)
                GlobalData::status->teamTurn = Team::Red;
            else if(GlobalData::status->teamTurn == Team::Red)
                GlobalData::status->teamTurn = Team::Blue;
            GlobalData::status->roleTurn = PType::SpyMaster;
        }
        else if(GlobalData::status->roleTurn == PType::SpyMaster)
        {
            GlobalData::status->roleTurn = PType::Agent;
            GlobalData::status->currentClue = indice;
        }
        break;

        case Mod::Coop:
        if(GlobalData::status->roleTurn == PType::Agent)
        {
            GWord::discoverRed();
            GlobalData::status->decrInf(Team::Red);
            if(GlobalData::status->getInf(Team::Red) == 0)
            {
                endOfGame();
                return;
            }
            GlobalData::status->roleTurn = PType::SpyMaster;
        }
        else if(GlobalData::status->roleTurn == PType::SpyMaster)
        {
            GlobalData::status->roleTurn = PType::Agent;
            GlobalData::status->currentClue = indice;
        }
        break;
    }
    GlobalData::status->markUpdated();
}

void GameEngine::endOfGame()
{
    GlobalData::status->teamTurn=Team::NA;
    GlobalData::status->markUpdated();
}
