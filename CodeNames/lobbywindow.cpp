#include "lobbywindow.h"
#include "ui_lobbywindow.h"

LobbyWindow::LobbyWindow(QWidget *parent, int plrCount, bool host) :
    QMainWindow(parent),
    ui(new Ui::LobbyWindow)
{
    ui->setupUi(this);

    //On start le network
    GlobalData::network->start();

    if(host){
        PlayerLobbyWidget* current;
        this->ui->layoutListPlr->addWidget(current = new PlayerLobbyWidget(this, true));
        current->setEnabled(host);
        for(int i = 1; i < plrCount; i++){
            this->ui->layoutListPlr->addWidget(current = new PlayerLobbyWidget(this));
            current->setEnabled(false);
        }
    }
}



LobbyWindow::~LobbyWindow()
{
    delete ui;
}

void LobbyWindow::newClientConnected()
{

}

void LobbyWindow::clientJoinedHost(QList<Player> plrs)
{
    //Message reçu par un client lors de sa connexion
}

void LobbyWindow::receivedPlrDataChange(int n, Player plr)
{
    //Changement des informations d'un joueur
}

void LobbyWindow::localPlayerChanged()
{

}

void LobbyWindow::chatMessageReceived(QString msg)
{

}

void LobbyWindow::onBtnContinueClicked()
{
    //Démarrage de la partie
    CodeNamesWindow* gameWin = new CodeNamesWindow(qobject_cast<QWidget*>(this->parent()));

    gameWin->show();

    this->close();
    this->deleteLater();
}

int LobbyWindow::getConnectedPlayerCount()
{
    return 0;

}
