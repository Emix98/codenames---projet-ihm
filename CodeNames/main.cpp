#include "launcherwindow.h"
#include "globaldata.h"

#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    GlobalData::initOptions();

    LauncherWindow w;
    w.show();

    return a.exec();
}
