#include "clientnetwork.h"

ClientNetwork::ClientNetwork(QString address, int port)
{
    this->address = address;
    this->port = port;

    // Création de la socket de connection avec le server
    this->socket = new QTcpSocket;


    // Une fois l'initialisation terminée, on démarre le QThread qui s'occupera de gérer la connectique
    //NOTE: les connections des signaux/slot se font dans ce thread
    //this->start();
}


int ClientNetwork::getPort()
{
    return this->port;
}

QList<QString> *ClientNetwork::getAdresses()
{
    QList<QString>* addresses = new QList<QString>;
    addresses->append(this->address);
    return addresses;
}

bool ClientNetwork::isReady()
{
    return this->ready;
}

ClientNetwork::~ClientNetwork()
{
    //On arrète la connexion
    this->socket->abort();
    delete this->socket;
}

void ClientNetwork::run() {
    //On déplace l'objet socket dans ce thread pour que les évenements soient éxécutés ici
    this->socket->moveToThread(this);

    // Connections
    connect(this->socket, SIGNAL(connected()), this, SLOT(onConnection()));
    connect(this->socket, SIGNAL(readyRead()), this, SLOT(onDataReceived()));
    connect(this->socket, SIGNAL(disconnected()), this, SLOT(onDisconnection()));
    connect(this->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onSocketError(QAbstractSocket::SocketError)));



    //OK, ça c'est dégeulasse, mais pour l'instant, ça va faire l'affaire
    //J'ai pas envie de tout réécrire
    //Permet au signaux ayant pour cible cet objet d'être lancés dans ce thread
    //cad ceux ci-dessus
    this->moveToThread(this);


    // Connection au server
    this->socket->connectToHost(address, port);

    //Et on finit par lancer la boucle évenementielle
    this->exec();
}



/*
 *
 * Implémentaiton des slots
 *
 */

void ClientNetwork::sendChatMessage(QString msg)
{
    QByteArray data = msg.toUtf8();
    this->sendData(NetworkMessage::CHAT_MESSAGE, data);
}


void ClientNetwork::onDataReceived()
{
    QDataStream stream(this->socket);

    quint32 packetSize;

    //On vérifie si le flux contient assez de données

    if(socket->bytesAvailable()< (int)sizeof(quint32)){
        //TODO: gestion de l'erreur
        return;
    }

    stream>>packetSize;

    //On vérifie qu'il y a suffisement de données dans la socket
    if(socket->bytesAvailable() < packetSize){
        //TODO: gestion de l'erreur
        return;
    }

    // On lit le message
    NetworkMessage message;
    stream.readRawData((char*)&message, 1);

    QByteArray data = socket->read(packetSize-sizeof(NetworkMessage));
    Team tea;

    switch(message){
    //TODO: traitement des messages et redirection vers la bonne partie
    case NetworkMessage::CHAT_MESSAGE:
        emit onChatMessageReceived(QString::fromUtf8(data));
        break;
    case NetworkMessage::CHAT_MESSAGE_TEAM:
        tea = static_cast<Team>(data.at(0));
        data.remove(0, 1);
        emit onChatMessageTeamReceived(QString::fromUtf8(data), tea);
        break;
    default:
        emit networkError(NetworkError::UNKNOW_MESSAGE);
    }


}

void ClientNetwork::onConnection()
{
    this->ready = true;
    emit networkReady();
}

void ClientNetwork::onDisconnection()
{
    this->ready = false;
    emit networkClosed();
}

void ClientNetwork::onSocketError(QAbstractSocket::SocketError error)
{
    NetworkError netError;

    switch(error){
    case QAbstractSocket::HostNotFoundError:
        netError = NetworkError::HOST_NOT_FOUND;
        break;
    case QAbstractSocket::ConnectionRefusedError:
        netError = NetworkError::CONNECTION_REFUSED;
        break;
    case QAbstractSocket::RemoteHostClosedError:
        netError = NetworkError::REMOTE_HOST_CLOSED_CONNECTION;
        break;
    default:
        netError = NetworkError::OTHER_SOCKET_ERROR;
        break;
    }

    emit networkError(netError);
}

void ClientNetwork::sendData(NetworkMessage msg, QByteArray data)
{
    QByteArray packet;
    QDataStream stream(&packet, QIODevice::WriteOnly);
    // On fait de la place pour pouvoir mettre le flux
    stream<<(quint32)0;
    stream<<msg;
    //On copie les données dans le flux
    stream.writeRawData(data.data(), data.size());
    //Ensuite on écrit la taille du packet
    //On se remet au début
    stream.device()->seek(0);
    //Püis on écrit la taille
    stream<<(quint32)(packet.size() - sizeof(quint32));

    //Et finalement on envoie le packet
    socket->write(packet);
}

void ClientNetwork::sendTeamChatMessage(QString msg, Team tea)
{
    QByteArray data;
    data.append(tea);
    data.append(msg.toUtf8());
    this->sendData(NetworkMessage::CHAT_MESSAGE_TEAM, data);
}



