#include "dialogaide.h"
#include "ui_dialogaide.h"

DialogAide::DialogAide(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAide)
{
    ui->setupUi(this);
}

DialogAide::~DialogAide()
{
    delete ui;
}

void DialogAide::on_listeSujet_itemClicked(QListWidgetItem *item)
{
    //On récupère le nom du fichier d'aide voulu
    QString helpFileName(":/texts/texts/");

    switch(this->ui->listeSujet->currentRow()) {
        case 0:
            helpFileName += "agents_";
            break;
        case 1:
            helpFileName += "master_";
            break;
        case 2:
            helpFileName += "game_";
            break;
        case 3:
            helpFileName += "tour_";
            break;
        default:
            return; //Rien de sélectionné, on arrête la méthode maintenant
    }

    if (GlobalData::options["lang"] == "Français") {
        helpFileName += "fr.html";
    } else if (GlobalData::options["lang"] == "English") {
        helpFileName += "en.html";
    }

    //Puis on récupère son contenu
    QFile helpFile(helpFileName);
    if (!helpFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        ui->displayZone->setText("..."); //On met un texte par défaut si on ne trouve pas le fichier
    }

    QString help("");
    QByteArray byteHelp;
    while (!helpFile.atEnd()) {
        byteHelp = helpFile.readLine();
        help += QString::fromUtf8(byteHelp);
    }
    //Et enfin on l'affiche dans la boîte à droite
    ui->displayZone->setText(help);
    helpFile.close();
}
