#ifndef CODENAMESWINDOW_H
#define CODENAMESWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QApplication>
#include <QMessageBox>
#include <QDebug>

#include "globaldata.h"
#include "gameengine.h"

#include "word.h"
#include "gword.h"

#include "dialogoptions.h"

#include "network.h"
#include "servernetwork.h"
#include "fakenetwork.h"
#include "clientnetwork.h"
#include "wordwidget.h"

namespace Ui {
class CodeNamesWindow;
}

class CodeNamesWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CodeNamesWindow(QWidget *parent = nullptr);
    ~CodeNamesWindow();

signals:
    void tileClicked(int);
    void turnEnded();

private slots:
    void gameStatusUpdated();

    void on_btnChat_clicked();

    void on_btnParam_clicked();

    void on_btnValider_clicked();

    void on_word_clicked();

private:
    Ui::CodeNamesWindow *ui;
    void setupWordWidget(WordWidget* w, int n);
    WordWidget *getWordWidget(int n);

};

#endif // CODENAMESWINDOW_H
