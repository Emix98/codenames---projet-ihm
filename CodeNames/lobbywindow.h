#ifndef LOBBYWINDOW_H
#define LOBBYWINDOW_H

#include <QMainWindow>

#include "playerlobbywidget.h"
#include "player.h"
#include "gplayer.h"
#include "codenameswindow.h"
#include "globaldata.h"

namespace Ui {
class LobbyWindow;
}

class LobbyWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LobbyWindow(QWidget *parent = nullptr, int plrCount = 0, bool host = false);
    ~LobbyWindow();

public slots:
    void newClientConnected();
    void clientJoinedHost(QList<Player> plrs);
    void receivedPlrDataChange(int n, Player plr);
    void localPlayerChanged();
    void chatMessageReceived(QString msg);
    void onBtnContinueClicked();

public:
    int getConnectedPlayerCount();

private:
    Ui::LobbyWindow *ui;
    int plrNum = 0;
};

#endif // LOBBYWINDOW_H
