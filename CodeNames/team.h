#ifndef TEAM_H
#define TEAM_H

enum Team : unsigned char
{
    Blue,
    Red,
    Witness,
    Assassin,
    NA
};

#endif // TEAM_H
