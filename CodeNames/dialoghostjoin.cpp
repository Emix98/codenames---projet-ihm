#include "dialoghostjoin.h"
#include "ui_dialoghostjoin.h"

DialogHostJoin::DialogHostJoin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogHostJoin)
{
    ui->setupUi(this);
}

DialogHostJoin::~DialogHostJoin()
{
    delete ui;
}

void DialogHostJoin::on_btnHost_clicked()
{
    //On change cette valeur pour ne pas envoyer le signal qui nous fais réafficher le launcher
    continuing = true;

    //On crée le serveur mais on le démarre pas encore
    GlobalData::network = new ServerNetwork(this->ui->spPortServ->value());


    //On ouvre la fenètre de configuration
    SetupWindow *setup = new SetupWindow(qobject_cast<QWidget*>(this->parent()), true);
    setup->show();

    //Et on ferme et supprime cette fenètre
    this->close();
    this->deleteLater();

}

void DialogHostJoin::closeEvent(QCloseEvent *ev)
{
    if(continuing){
        emit windowClosingContinue();
    } else {
        emit windowClosingCancel();
    }
    QDialog::closeEvent(ev);
}

void DialogHostJoin::on_btnJoin_clicked()
{
    //On change la valeur pour ne pas renvoyer le mauvais signal
    continuing = true;

    //On prépare la connexion au serveur, sans la démarrer pour autant
    GlobalData::network = new ClientNetwork(this->ui->leAddress->text(), this->ui->spPortClient->value());

    //On ouvre la fenètre de lobby
    LobbyWindow* lobby = new LobbyWindow(qobject_cast<QWidget*>(this->parent()));
    lobby->show();

    //Et on ferme cette fenètre
    this->close();
    this->deleteLater();
}
