#include "gplayer.h"
QList<Player*> GPlayer::playerList;


Player *GPlayer::createPlayer(QString n, Team t, PType pT)
{
    Player *p = nullptr;
    p = new Player(n,t,pT);
    GPlayer::playerList.append(p);
    return p;
}

Player *GPlayer::getPlayerById(int id)
{
    for(int i=0; i<GPlayer::playerList.length(); i++){
        if (GPlayer::playerList[i]->id==id)
            return GPlayer::playerList[i];
    }
    return nullptr;
}

QList<Player *> GPlayer::getPlayerByName(QString name)
{
    QList<Player*> res;
    for(int i=0; i<GPlayer::playerList.length(); i++){
        if (GPlayer::playerList[i]->name==name)
            GPlayer::playerList.append(GPlayer::playerList[i]);
    }
    return res;
}

QList<Player *> GPlayer::getPlayers()
{
    return GPlayer::playerList;
}

QList<Player *> GPlayer::getBlues()
{
    QList<Player*> res;
    for(int i=0; i<GPlayer::playerList.length(); i++){
        if (GPlayer::playerList[i]->team==Team::Blue)
            GPlayer::playerList.append(GPlayer::playerList[i]);
    }
    return res;
}

QList<Player *> GPlayer::getReds()
{
    QList<Player*> res;
    for(int i=0; i<GPlayer::playerList.length(); i++){
        if (GPlayer::playerList[i]->team==Team::Red)
            GPlayer::playerList.append(GPlayer::playerList[i]);
    }
    return res;
}

int GPlayer::deletePlayer(int id)
{
    Player *p = GPlayer::getPlayerById(id);
    if(p==nullptr)
        return -1;
    GPlayer::playerList.removeOne(p);
    delete(p);
    return 0;
}
