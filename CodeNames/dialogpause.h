#ifndef DIALOGPAUSE_H
#define DIALOGPAUSE_H

#include <QDialog>

namespace Ui {
class DialogPause;
}

class DialogPause : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPause(QWidget *parent = nullptr);
    ~DialogPause();

private:
    Ui::DialogPause *ui;
};

#endif // DIALOGPAUSE_H
