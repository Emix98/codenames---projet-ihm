#ifndef DIALOGOPTIONS_H
#define DIALOGOPTIONS_H

#include <QDialog>
#include <QTranslator>

#include "globaldata.h"
#include "launcherwindow.h"

namespace Ui {
class DialogOptions;
}

class DialogOptions : public QDialog
{
    Q_OBJECT

public:
    explicit DialogOptions(QWidget *parent = 0);
    ~DialogOptions();

private slots:
    void on_cboLang_currentTextChanged(const QString &arg1);

private:
    Ui::DialogOptions *ui;
};

#endif // DIALOGOPTIONS_H
