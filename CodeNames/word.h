#ifndef WORD_H
#define WORD_H

#include <QString>
#include <team.h>
class Word
{
public:
    QString value;

    /**
     * @brief Constructeur par défaut pour faire des tableaux
     */
    Word();

    /**
     * @brief Word : Constructeur
     * @param val : texte représentant le mot
     * @param team : équpie du mot
     */
    Word(QString val, Team team);

    /**
     * @brief discover : "Retourne" le mot et renvoie l'équipe à laquelle il appartient
     * @return équpie du mot
     */
    Team discover();

    /**
     * @brief isDiscovered : accesseur
     * @return valeur de discovered
     */
    bool isDiscovered();

    /**
     * @brief copyFrom : met à jour les champs du mot pour correspondre au mot en paramètre
     * @param w : mot à copier
     */
    void copyFrom(Word w);

    Team getTeam();

    /**
     * Destructeur
     */
    ~Word();

private:
    bool discovered;
    Team team;
};

#endif // WORD_H
