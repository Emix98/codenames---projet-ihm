#ifndef SETUPWINDOW_H
#define SETUPWINDOW_H

#include <QMainWindow>

#include <QDebug>

#include "lobbywindow.h"
#include "codenameswindow.h"

#include "gamestatus.h"

#include "globaldata.h"

#include "fakenetwork.h"
#include "servernetwork.h"

#include "gword.h"

namespace Ui {
class SetupWindow;
}

class SetupWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SetupWindow(QWidget *parent = 0, bool multiplayer = false);
    ~SetupWindow();
signals:
    void windowClosing();

private slots:
    void on_btnStart_clicked();
    void onSpinBoxValueChanged();
private:
    Ui::SetupWindow *ui;
    void closeEvent(QCloseEvent* ev);
    bool isMultiplayer;
    int oldSpValues[3];
    void setupGame();
    bool sendCloseEvent = true;
};

#endif // SETUPWINDOW_H
