#ifndef DIALOGAIDE_H
#define DIALOGAIDE_H

#include <QDialog>
#include <QListWidgetItem>

#include "globaldata.h"

namespace Ui {
class DialogAide;
}

class DialogAide : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAide(QWidget *parent = nullptr);
    ~DialogAide();

private slots:
    //void on_listeSujet_currentRowChanged(int currentRow);

    //void on_listeSujet_itemSelectionChanged();

    void on_listeSujet_itemClicked(QListWidgetItem *item);

private:
    Ui::DialogAide *ui;
};

#endif // DIALOGAIDE_H
