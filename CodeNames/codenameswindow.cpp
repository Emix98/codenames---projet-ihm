#include "codenameswindow.h"
#include "ui_codenameswindow.h"

//Normalement je ne ferait pas ça, mais on a une référence circulaire donc bon...
#include "icontentwidget.h"

CodeNamesWindow::CodeNamesWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CodeNamesWindow)
{
    ui->setupUi(this);

    connect(GlobalData::status, SIGNAL(updated()), this, SLOT(gameStatusUpdated()));


    //Setup des widget
    setupWordWidget(this->ui->c00, 0);
    setupWordWidget(this->ui->c01, 1);
    setupWordWidget(this->ui->c02, 2);
    setupWordWidget(this->ui->c03, 3);
    setupWordWidget(this->ui->c04, 4);
    setupWordWidget(this->ui->c10, 5);
    setupWordWidget(this->ui->c11, 6);
    setupWordWidget(this->ui->c12, 7);
    setupWordWidget(this->ui->c13, 8);
    setupWordWidget(this->ui->c14, 9);
    setupWordWidget(this->ui->c20, 10);
    setupWordWidget(this->ui->c21, 11);
    setupWordWidget(this->ui->c22, 12);
    setupWordWidget(this->ui->c23, 13);
    setupWordWidget(this->ui->c24, 14);
    setupWordWidget(this->ui->c30, 15);
    setupWordWidget(this->ui->c31, 16);
    setupWordWidget(this->ui->c32, 17);
    setupWordWidget(this->ui->c33, 18);
    setupWordWidget(this->ui->c34, 19);
    setupWordWidget(this->ui->c40, 20);
    setupWordWidget(this->ui->c41, 21);
    setupWordWidget(this->ui->c42, 22);
    setupWordWidget(this->ui->c43, 23);
    setupWordWidget(this->ui->c44, 24);

    //On force l'appel à GameStatusUpdated pour tout afficher correctement
    gameStatusUpdated();
}

CodeNamesWindow::~CodeNamesWindow()
{
}

void CodeNamesWindow::gameStatusUpdated()
{


    GameStatus* sts = GlobalData::status;
    if(sts->roleTurn== PType::Murdered)
    {
        if(sts->teamTurn == Team::Red)
            QMessageBox::warning(this, tr("Fin de partie"), tr("L'équipe rouge a trouvé l'assassin, elle a perdu..."));
        else if(sts->teamTurn == Team::Blue)
            QMessageBox::warning(this, tr("Fin de partie"), tr("L'équipe bleue a trouvé l'assassin, elle a perdu..."));
        deleteLater();

    }
    else if(sts->teamTurn== Team::NA)
    {
        if(sts->getInf(Team::Red) == 0)
            QMessageBox::warning(this, tr("Fin de partie"), tr("L'équipe rouge a gagné !"));
        else if(sts->getInf(Team::Blue) == 0)
            QMessageBox::warning(this, tr("Fin de partie"), tr("L'équipe bleue a gagné !"));
        deleteLater();
    }
    if(sts->roleTurn== PType::SpyMaster)
        QMessageBox::warning(this, tr("Attention"), tr("Tour d'un maître espion, vérifiez qu'aucun agent ne voit l'écran !"));
    this->ui->labelEspRestBleu->setText(QString::number(sts->blueInformant));
    this->ui->labelEspRestRed->setText(QString::number(sts->redInformant));
    if(sts->roleTurn== PType::Agent)
    {
       this->ui->btnValider->setText(tr("Fin de tour"));
       this->ui->btnValider->setEnabled(false);
       this->ui->leIndice->setReadOnly(true);
        this->ui->spEspionRestants->setReadOnly(true);

    }
    else
    {
        this->ui->btnValider->setText(tr("Valider"));
        this->ui->btnValider->setEnabled(true);
        this->ui->leIndice->setReadOnly(false);
        this->ui->spEspionRestants->setReadOnly(false);
        this->ui->leIndice->setText("");
        this->ui->spEspionRestants->setValue(1);
        if(sts->teamTurn == Team::Blue)
            this->ui->spEspionRestants->setMaximum(sts->blueInformant);
        else if(sts->teamTurn == Team::Red)
            this->ui->spEspionRestants->setMaximum(sts->redInformant);

    }
    QPixmap pix = QPixmap::fromImage(QImage(sts->teamTurn == Team::Red ? ":/images/images/turn_red.png" :":/images/images/turn_blue.png"));

    this->ui->labelSymbEquipeAct->setScaledContents(true);
    this->ui->labelSymbEquipeAct->setPixmap(pix);//.scaled(this->ui->labelSymbEquipeAct->height(), this->ui->labelSymbEquipeAct->width()));

    QString maitreEsp = tr("Maitre Espion");
    QString agent = tr("Agent");
    QString red = tr("Rouge");
    QString blue = tr("Bleu");
    this->ui->labelJoueurActuel->setText(QString("")+(sts->roleTurn == PType::Agent ? agent : maitreEsp)+ QString(" ") + (sts->teamTurn == Team::Red ? red : blue));


    //Affichage des words :
    for(int i = 0; i < 25; i++){
        WordWidget* wdg = getWordWidget(i);
        Word* wrd = GWord::getWord(i);
        wdg->displayWord(wrd, sts->roleTurn == PType::Agent);
    }
}

void CodeNamesWindow::setupWordWidget(WordWidget *w, int n)
{
    w->number = n;
    //connect(w, SIGNAL(clicked(int)), this, SIGNAL(tileClicked(int)));
    connect(w, &WordWidget::clicked, GameEngine::discover);
    connect(w,SIGNAL(clicked(int)),this,SLOT(on_word_clicked()));
}

WordWidget *CodeNamesWindow::getWordWidget(int n)
{
    if(n == 0) return this->ui->c00;
    if(n == 1) return this->ui->c01;
    if(n == 2) return this->ui->c02;
    if(n == 3) return this->ui->c03;
    if(n == 4) return this->ui->c04;
    if(n == 5) return this->ui->c10;
    if(n == 6) return this->ui->c11;
    if(n == 7) return this->ui->c12;
    if(n == 8) return this->ui->c13;
    if(n == 9) return this->ui->c14;
    if(n == 10) return this->ui->c20;
    if(n == 11) return this->ui->c21;
    if(n == 12) return this->ui->c22;
    if(n == 13) return this->ui->c23;
    if(n == 14) return this->ui->c24;
    if(n == 15) return this->ui->c30;
    if(n == 16) return this->ui->c31;
    if(n == 17) return this->ui->c32;
    if(n == 18) return this->ui->c33;
    if(n == 19) return this->ui->c34;
    if(n == 20) return this->ui->c40;
    if(n == 21) return this->ui->c41;
    if(n == 22) return this->ui->c42;
    if(n == 23) return this->ui->c43;
    if(n == 24) return this->ui->c44;
    //Default
    return nullptr;
}

void CodeNamesWindow::on_btnChat_clicked()
{
    QMessageBox::warning(this, tr("Attention"), tr("Le chat n'est pas disponible en partie locale"));
}

void CodeNamesWindow::on_btnParam_clicked()
{
    //On affiche le dialogue d'aide, qui sera supprimé à sa fermeture
    DialogAide* aide = new DialogAide(this);
    aide->show();
    aide->setModal(false);
    aide->setAttribute(Qt::WA_DeleteOnClose);
}

void CodeNamesWindow::on_btnValider_clicked()
{
    if(!(this->ui->leIndice->text() == nullptr)){
        GameEngine::indice = this->ui->leIndice->text();
        GameEngine::endOfTurn();

    }else{
         QMessageBox::warning(this, tr("Attention"), tr("Indice vide !"));
    }
}

void CodeNamesWindow::on_word_clicked()
{
    this->ui->spEspionRestants->setValue(this->ui->spEspionRestants->value()-1);
    this->ui->btnValider->setEnabled(true);
}
