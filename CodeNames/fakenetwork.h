#ifndef FAKENETWORK_H
#define FAKENETWORK_H

#include "network.h"

class FakeNetwork : public Network
{
    //Permet le support des signaux/slots
    Q_OBJECT
public:
    /**
     * @brief FakeNetwork instancie une connectique locale
     */
    FakeNetwork();
    /**
     * @brief getPort
     * @return Renvoie toujours 0
     */
    virtual int getPort();
    /**
     * @brief getAdresses
     * @return Renvoie toujours une liste vide qui doit être libérée par la suite (via delete)
     */
    virtual QList<QString>* getAdresses();
    /**
     * @brief isReady
     * @return Renvoie toujours vrai
     */
    virtual bool isReady();

    ~FakeNetwork();

public slots:
    /**
     * @brief sendChatMessage : slot utilisé pour envoyer un message, se contente de le passer au signal de message reçu
     * @param msg : le message
     */
    virtual void sendChatMessage(QString msg);

    /**
     * @brief sendTeamChatMessage : permet d'envoyer un message de chat à une équipe
     * @param msg : le message à transmettre
     * @param tea : l'équipe de destination du message
     */
    virtual void sendTeamChatMessage(QString msg, Team tea);
    //TODO: Redéfinitions de chaques slots de gestions des messages venant de network

private:
    /**
     * @brief run : méthode démarrée avec le début du thread, ne fait rien de particulier ici
     */
    virtual void run();

};

#endif // FAKENETWORK_H
