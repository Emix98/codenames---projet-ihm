#include "gamestatus.h"

GameStatus::GameStatus(QObject *parent, Mod mode, int blueInf, int redInf, Team curTeamTurn, PType curRoleTurn, QString curClue, int nbWForClue) : QObject(parent)
{
    mod = mode;
    blueInformant = blueInf;
    redInformant = redInf;
    teamTurn = curTeamTurn;
    roleTurn = curRoleTurn;
    currentClue = curClue;
    nbWordsForClue = nbWForClue;
}

QByteArray GameStatus::serialize()
{
    QString status = "";
    status += mod + ";";
    status += blueInformant + ";";
    status += redInformant + ";";
    status += teamTurn + ";";
    status += roleTurn + ";";
    status += currentClue + ";";
    status += nbWordsForClue;
    return status.toUtf8();
}

void GameStatus::updateViaString(QByteArray byteStatus)
{
    QString status(QString::fromUtf8(byteStatus));
    QStringList attributes(status.split(';'));
    mod = (Mod)attributes.at(0).toInt();
    blueInformant = attributes.at(1).toInt();
    redInformant = attributes.at(2).toInt();
    teamTurn = (Team) attributes.at(3).toInt();
    roleTurn = (PType) attributes.at(4).toInt();
    currentClue = attributes.at(5);
    nbWordsForClue = attributes.at(6).toInt();

    emit updated();
}

GameStatus* GameStatus::unserialize(QByteArray byteStatus)
{
    QString statusString(QString::fromUtf8(byteStatus));
    QStringList attributes(statusString.split(';'));
    return new GameStatus(nullptr, (Mod)attributes.at(0).toInt(), attributes.at(1).toInt(), attributes.at(2).toInt(), (Team) attributes.at(3).toInt(), (PType) attributes.at(4).toInt(), attributes.at(5), attributes.at(6).toInt());
}

void GameStatus::decrInf(Team t)
{
    if(t == Team::Blue)
        blueInformant--;
    else if(t == Team::Red)
        redInformant--;
}

int GameStatus::getInf(Team t)
{
    if(t == Team::Blue)
        return blueInformant;
    if(t == Team::Red)
        return redInformant;
    else
        return Team::NA;
}

void GameStatus::markUpdated()
{
    emit updated();
}
