#ifndef SERVERNETWORK_H
#define SERVERNETWORK_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QNetworkInterface>

#include <QByteArray>
#include <QDataStream>

#include "network.h"

/**
 * @brief Implémentation de la classe Network pour le serveur
 * Se crée en instanciant simplement
 * Se démarre en utilisant la méthode start
 * Se termine en utilisant la méthode quit
 */
class ServerNetwork : public Network
{
    // Nécessaire pour supporter les signaux/slots customs
    Q_OBJECT

public:
    /**
     * @brief ServerNetwork crée une nouvelle instance avec un serveur sur le port indiqué
     * @param port : le numéro de port à écouter
     */
    ServerNetwork(int port);

    /**
     * @brief getPort
     * @return Le port de connection à la partie
     */
    virtual int getPort();
    /**
     * @brief getAdresses Renvoie la liste d'addresses de connection possible au serveur (sur toutes les interfaces)
     * @return la liste des addresses possibles qui doit être libérée par la suite (via delete)
     */
    virtual QList<QString> *getAdresses();
    /**
     * @brief isReady
     * @return indique si la connectique est prète et à finit de s'initialisée
     */
    virtual bool isReady();

    /**
      * Destructeur de la classe
      */
    ~ServerNetwork();

public slots:
    /**
     * @brief broadcastData envoie un message à tout les clients connectés au serveur
     * @param msg : le type de message
     * @param data : les données associées au message
     */
    void broadcastData(NetworkMessage msg, QByteArray data);
    /**
     * @brief sendDataToClient envoie un message à un client spécifique
     * @param client : la socket du client cible
     * @param msg : le type du message
     * @param data : les données associées au message
     */
    void sendDataToClient(QTcpSocket*client, NetworkMessage msg, QByteArray data);
    /**
     * @brief sendChatMessage : implémentation du serveur d'envoie d'un message de chat
     * @param msg : le contenu du message à envoyer
     */
    virtual void sendChatMessage(QString msg);
    /**
     * @brief sendTeamChatMessage : permet d'envoyer un message de chat à une équipe
     * @param msg : le message à transmettre
     * @param tea : l'équipe de destination du message
     */
    virtual void sendTeamChatMessage(QString msg, Team tea);

private:
    /**
     * @brief port : le port sur lequel tourne le serveur
     */
    int port;
    /**
     * @brief ready : si la connectique est prête
     */
    bool ready = false;
    /**
     * @brief server : Le serveur gérant les connections TCP
     */
    QTcpServer* server;
    /**
     * @brief clientSockets : L'ensemble des sockets TCP correspondant aux clients
     */
    QList<QTcpSocket*> clientSockets;

    /**
     * @brief createPacket : fonction utilitaire pour crée un paquet pour les informations passés en paramètre
     * @param msg : le type du message
     * @param data : les données associées
     * @return Le paquet pret à être envoyer
     */
    QByteArray createPacket(NetworkMessage msg, QByteArray* data);

    /**
     * @brief run : méthode démarrée dans un autre thread, démarrant le serveur à proprement parlé
     */
    virtual void run();

private slots:
    /**
     * @brief onDataReceived : slot appelé en cas de récéption de données
     */
    void onDataReceived();
    /**
     * @brief onNewConnection : slot appelé en cas de nouvelle connection de client
     */
    void onNewConnection();
    /**
     * @brief onDisconnection : slot appelé en cas de déconnection de socket
     */
    void onDisconnection();
    /**
     * @brief onServerError : slot appelé en cas d'erreur sur les sockets
     * @param error : l'erreur levée
     */
    void onServerError(QAbstractSocket::SocketError error);


};

#endif // SERVERNETWORK_H
