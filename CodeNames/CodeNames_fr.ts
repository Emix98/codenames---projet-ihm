<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ChatWindow</name>
    <message>
        <location filename="chatwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="36"/>
        <source>Global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="52"/>
        <location filename="chatwindow.ui" line="76"/>
        <location filename="chatwindow.ui" line="100"/>
        <source>Envoyer</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="60"/>
        <source>Equipe</source>
        <translation>Équipe</translation>
    </message>
    <message>
        <location filename="chatwindow.ui" line="84"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
</context>
<context>
    <name>CodeNamesWindow</name>
    <message>
        <location filename="codenameswindow.ui" line="14"/>
        <source>CodeNamesWindow</source>
        <translation>Code Name</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Equipe&lt;br/&gt;bleue&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Équipe&lt;br/&gt;bleue&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="148"/>
        <source>Espions Restants :</source>
        <translation>Espions Restans :</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="171"/>
        <location filename="codenameswindow.cpp" line="87"/>
        <source>Valider</source>
        <translation>Valider</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="197"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="220"/>
        <location filename="codenameswindow.ui" line="318"/>
        <source>Nbespionb</source>
        <translation>Nombre d&apos;espions</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="240"/>
        <source>étape en cours</source>
        <translation>Étape en cours</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="275"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Param</source>
        <translation type="vanished">Options</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="295"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Equipe&lt;br/&gt;Rouge&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Equipe&lt;br/&gt;Rouge&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="338"/>
        <source>Indice :</source>
        <translation>Indice :</translation>
    </message>
    <message>
        <location filename="codenameswindow.ui" line="354"/>
        <source>Symbole d&apos;équipe actuelle</source>
        <translation>Symbole d&apos;équipe actuelle</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="59"/>
        <location filename="codenameswindow.cpp" line="61"/>
        <location filename="codenameswindow.cpp" line="68"/>
        <location filename="codenameswindow.cpp" line="70"/>
        <source>Fin de partie</source>
        <translation>Fin de partie</translation>
    </message>
    <message>
        <source>L&apos;équipe rouge a perdu...</source>
        <translation type="vanished">L&apos;équipe rouge a perdu...</translation>
    </message>
    <message>
        <source>L&apos;équipe bleue a perdu...</source>
        <translation type="vanished">L&apos;équipe bleue a perdu...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="59"/>
        <source>L&apos;équipe rouge a trouvé l&apos;assassin, elle a perdu...</source>
        <translation>L&apos;équipe rouge a trouvé l&apos;assassin, elle a perdu...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="61"/>
        <source>L&apos;équipe bleue a trouvé l&apos;assassin, elle a perdu...</source>
        <translation>L&apos;équipe bleue a trouvé l&apos;assassin, elle a perdu...</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="68"/>
        <source>L&apos;équipe rouge a gagné !</source>
        <translation>L&apos;équipe rouge a gagné !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="70"/>
        <source>L&apos;équipe bleue a gagné !</source>
        <translation>L&apos;équipe bleue a gagné !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="74"/>
        <location filename="codenameswindow.cpp" line="160"/>
        <location filename="codenameswindow.cpp" line="179"/>
        <source>Attention</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="74"/>
        <source>Tour d&apos;un maître espion, vérifiez qu&apos;aucun agent ne voit l&apos;écran !</source>
        <translation>Tour d&apos;un maître espion, vérifiez qu&apos;aucun agent ne voit l&apos;écran !</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="79"/>
        <source>Fin de tour</source>
        <translation>Fin de tour</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="104"/>
        <source>Maitre Espion</source>
        <translation>Maitre Espion</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="105"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="106"/>
        <source>Rouge</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="107"/>
        <source>Bleu</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="160"/>
        <source>Le chat n&apos;est pas disponible en partie locale</source>
        <translation>Le chat n&apos;est pas disponible en partie locale</translation>
    </message>
    <message>
        <location filename="codenameswindow.cpp" line="179"/>
        <source>Indice vide !</source>
        <translation>Indice vide !</translation>
    </message>
</context>
<context>
    <name>DialogAide</name>
    <message>
        <location filename="dialogaide.ui" line="14"/>
        <source>CodeNames: Aide</source>
        <translation>CodeNames: Aide</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="27"/>
        <source>Agents</source>
        <translation>Agents</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="32"/>
        <source>Maître-espion</source>
        <translation>Maître-Espion</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="37"/>
        <source>Jeu</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="42"/>
        <source>Tour</source>
        <translation>Tour</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="56"/>
        <source>Fermer</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Cliquez sur l&apos;une des options dans la liste pour en apprendre plus sur les différents aspects de CodeNames !&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Cliquez sur l&apos;une des options dans la liste pour en apprendre plus sur les différents aspects de CodeNames !&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dialogaide.ui" line="89"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>DialogHostJoin</name>
    <message>
        <location filename="dialoghostjoin.ui" line="14"/>
        <source>Dialog</source>
        <translation>Héberger/Rejoindre</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="39"/>
        <location filename="dialoghostjoin.ui" line="148"/>
        <source>Héberger</source>
        <translation>Héberger</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="45"/>
        <source>Nom de la partie :</source>
        <translation>Nom de la partie :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="55"/>
        <source>Entrez un nom</source>
        <translation>Entrez un nom</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="75"/>
        <source>Nombre de joueurs :</source>
        <translation>Nombre de joueurs :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="102"/>
        <source>Port d&apos;écout e:</source>
        <translation>Port d&apos;écoute :</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="169"/>
        <location filename="dialoghostjoin.ui" line="221"/>
        <source>Rejoindre</source>
        <translation>Rejoindre</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="175"/>
        <source>Adresse de l&apos;hôte</source>
        <translation>Adresse de l&apos;hôte</translation>
    </message>
    <message>
        <location filename="dialoghostjoin.ui" line="198"/>
        <source>Port de connexion</source>
        <translation>Port de connexion</translation>
    </message>
</context>
<context>
    <name>DialogOptions</name>
    <message>
        <location filename="dialogoptions.ui" line="14"/>
        <source>CodeNames: Options</source>
        <translation>CodeNames : Options</translation>
    </message>
    <message>
        <location filename="dialogoptions.ui" line="22"/>
        <source>Langue :</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location filename="dialogoptions.ui" line="45"/>
        <source>Fermer</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>DialogPause</name>
    <message>
        <location filename="dialogpause.ui" line="14"/>
        <source>Dialog</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="64"/>
        <source>Jeu en pause</source>
        <translation>Jeu En Pause</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="79"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="91"/>
        <source>Reprendre</source>
        <translation>Reprendre</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="103"/>
        <source>Recommencer</source>
        <translation>Recommencer</translation>
    </message>
    <message>
        <location filename="dialogpause.ui" line="115"/>
        <source>Quitter</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>LauncherWindow</name>
    <message>
        <location filename="launcherwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>CodeNames Lanceur</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ceci est une image&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ceci est une image&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="45"/>
        <source>Partie locale</source>
        <translation>Partie Locale</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="55"/>
        <source>Partie en réseau</source>
        <translation>Partie en Réseau</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="62"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="69"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="launcherwindow.ui" line="76"/>
        <source>Quitter</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>LobbyWindow</name>
    <message>
        <location filename="lobbywindow.ui" line="14"/>
        <source>CodeName : Lobby</source>
        <translation>CodeName : Lobby</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="24"/>
        <source>Démarrer</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="44"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="60"/>
        <source>Envoyer</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location filename="lobbywindow.ui" line="70"/>
        <source>Addresse(s) du serveur :</source>
        <translation>Addresse(s) du serveur :</translation>
    </message>
</context>
<context>
    <name>PlayerLobbyWidget</name>
    <message>
        <location filename="playerlobbywidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="20"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="31"/>
        <source>Maitre</source>
        <translation>Maitre</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="36"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="45"/>
        <source>Rouge</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="50"/>
        <source>Bleue</source>
        <translation>Bleue</translation>
    </message>
    <message>
        <location filename="playerlobbywidget.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>SetupWindow</name>
    <message>
        <location filename="setupwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>CodeNames : Paramétrage d&apos;une partie</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="63"/>
        <source>Nombre d&apos;espions rouge</source>
        <translation>Nombre d&apos;espions rouges</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="109"/>
        <source>Nombre d&apos;espions bleu</source>
        <translation>Nombre d&apos;espions bleus</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="152"/>
        <source>Nombre d&apos;assassin</source>
        <translation>Nombre d&apos;assassin</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="178"/>
        <source>Aléatoire</source>
        <translation>Aléatoire</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="183"/>
        <source>Rouge</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="188"/>
        <source>Bleu</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="206"/>
        <source>Équipe de départ</source>
        <translation>Équipe de départ</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="252"/>
        <source>Témoins</source>
        <translation>Témoins</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="270"/>
        <source>Ne jouez qu&apos;une seule équipe (2 joueurs minimum)</source>
        <translation>Ne jouez qu&apos;une seule équipe (2 joueurs minimum)</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="273"/>
        <source>Coopératif</source>
        <translation>Coopératif</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="291"/>
        <source>Afrontez vous (4 joueurs minimum)</source>
        <translation>Affrontez vous (4 joueurs minimum)</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="294"/>
        <source>Compétitif</source>
        <translation>Compétitif</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="304"/>
        <source>Commencer</source>
        <translation>Commencer</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="316"/>
        <source>Choisissez le mode de jeu :</source>
        <translation>Choisissez le mode de jeu :</translation>
    </message>
    <message>
        <location filename="setupwindow.ui" line="333"/>
        <source>Personnalisation</source>
        <translation>Personnalisation</translation>
    </message>
</context>
<context>
    <name>WordWidget</name>
    <message>
        <location filename="wordwidget.ui" line="20"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
</TS>
