#ifndef NETWORK_H
#define NETWORK_H

#include <QThread>
#include <QList>
#include <QString>


#include "networkerrors.h"
#include "networkmessages.h"
#include "team.h"

/**
 * @brief La Classe Network
 * Classe abstraite représentant la connection entre un hôte/joueur durant une partie
 * Définie de manière concrète dans ServerNetwork pour un serveur, ClientNetwork pour un client et finalement, FakeNetwork pour une partie locale
 */
class Network : public QThread
{
    //Permet le support des signaux/slots
    Q_OBJECT

public:
    // Les différentes fonctions utilisées
    /**
     * @brief getMaxClientCount : donne le nombre maximum de joueur possible sur la partie
     * @return le nombre maximum de joueur possible dans la partie en cours de jeu/préparation
     */
    virtual int getMaxClientCount();
    /**
     * @brief getClientCount
     * @return Le nombre de joueur actuellement connecté dans la partie en cours de jeu/préparation
     */
    virtual int getClientCount();
    /**
     * @brief getPort
     * @return Le port de connection à la partie si applicable, 0 sinon
     */
    virtual int getPort() = 0;

    /**
     * @brief getAdresses
     * @return Une liste de chaines de caractères représentant l'/les addresse(s) du serveur qui doit être libérée par la suite (via delete)
     */
    virtual QList<QString> * getAdresses()=0;
    /**
     * @brief isReady
     * @return indique si la connectique est prète et à finit de s'initialisée
     */
    virtual bool isReady()=0;

// Ici vont tout les slots pour les envoie de messages entre client/serveur
public slots:
    /**
     * @brief sendChatMessage : permet d'envoyer un message de chat au travers de la connectique
     * @param msg : le message à envoyer
     */
    virtual void sendChatMessage(QString msg)=0;

    /**
     * @brief sendTeamChatMessage : permet d'envoyer un message de chat à une équipe
     * @param msg : le message à transmettre
     * @param tea : l'équipe de destination du message
     */
    virtual void sendTeamChatMessage(QString msg, Team tea) = 0;


signals://Les signaux d'E/S pourles interractions entre la connectique et l'interface
    /**
     * @brief onChatMessageReceived : signal levé quand un message de chat est reçu
     */
    void onChatMessageReceived(QString);
    /**
     * @brief networkReady : signal envoyé quand la connectique est prête
     */
    void networkReady();
    /**
     * @brief networkError : signal envoyé en cas d'erreur sur la connectique
     */
    void networkError(NetworkError);
    /**
     * @brief networkClosed : signal envoyé quand la connectique se termine ou ferme en cas de problèmes
     */
    void networkClosed();

    /**
     * @brief onChatMessageTeamReceived : signal envoyé quand un message de chat spécifique à une équipe est envoyé
     */
    void onChatMessageTeamReceived(QString, Team);
    //TODO: créer les signaux/slots correspondant à chaques messages entrant et sortant du réseau

protected:
    /**
     * @brief run : méthode démarrée dans un autre thread au lancement de celui-ci
     */
    virtual void run()=0;

    /**
     * @brief currentClientCount : nombre actuel de client connectés
     */
    int currentClientCount = 0;
    /**
     * @brief maxClientCount : nombre maximum de client connectés
     */
    int maxClientCount = 0;


};

#endif // NETWORK_H
