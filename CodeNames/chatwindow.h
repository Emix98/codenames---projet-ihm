#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QDialog>
#include <QScrollBar>

#include <QString>
#include <QStringBuilder>

#include "network.h"
#include "player.h"
#include "team.h"

namespace Ui {
class ChatWindow;
}

/**
 * @brief La fenètre d'affichage pour le chat
 * @author Alexandre Meyer
 */
class ChatWindow : public QDialog
{
    //Macro nécessaire pour les QObjects
    Q_OBJECT

public:
    /**
     * @brief ChatWindow : Constructeur de la fenètre de chat
     * @param parent : La fenètre parente
     * @param plrName : le nom du joueur
     * @param net : la connectique réseau
     */
    ChatWindow(QWidget *parent = nullptr, Player* plr = nullptr,Network* net = NULL);
    ~ChatWindow();

public slots:
    void receiveChatMessage(QString message);
    void receiveChatMessageTeam(QString message, Team tea);

signals:
    void newChatMessage(QString message);
    void newChatMessageTeam(QString message, Team tea);

private slots:
    void on_sendBtn_clicked();

private:
    Ui::ChatWindow *ui;
    Player* localPlayer;
    Network* network;
};

#endif // CHATWINDOW_H
