#include "word.h"

Word::Word()
{
    this->value="";
    this->team=Team::NA;
    this->discovered=false;
}

Word::Word(QString val, Team team) : value(val)
{
    this->discovered=false;
    this->team = team;

}


Team Word::discover()
{
    this->discovered=true;
    this->value="";
    return this->team;
}


bool Word::isDiscovered()
{
    return this->discovered;
}

void Word::copyFrom(Word w)
{
    this->value=w.value;
    this->discovered=w.discovered;
    this->team=w.team;
}

Team Word::getTeam()
{
    return this->team;
}

Word::~Word()
{

}
