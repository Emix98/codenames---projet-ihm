#include "fakenetwork.h"

FakeNetwork::FakeNetwork()
{
    //Pas grand chose à faire ici...
}

int FakeNetwork::getPort()
{
    return 0;
}

QList<QString> *FakeNetwork::getAdresses()
{
    return new QList<QString>();
}

bool FakeNetwork::isReady()
{
    return true;
}

FakeNetwork::~FakeNetwork()
{

}

void FakeNetwork::sendChatMessage(QString msg)
{
    //On se contente de passer le message
    emit onChatMessageReceived(msg);
}

void FakeNetwork::sendTeamChatMessage(QString msg, Team tea)
{
    //On se contente de passer le message
    emit onChatMessageTeamReceived(msg, tea);
}


void FakeNetwork::run()
{
    //on ne fait rien, on est donc déjà prêt
    emit networkReady();
}
