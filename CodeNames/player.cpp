#include "player.h"
int Player::cptPlayer = 0;

Player::Player(): name("anonymous")
{
    team = Team::Witness;
    pType = PType::Agent;
    id = Player::cptPlayer;
    Player::cptPlayer++;
}

Player::Player(const Player &other):
    name(other.name),
    team(other.team),
    pType(other.pType),
    id(other.id)
{
    //On incrémente pas cptPlayer, puisqu'on fais qu'une copie
}

Player::Player(QString n, Team t, PType pT): name(n)
{
    team = t;
    pType = pT;
    id = Player::cptPlayer;
    Player::cptPlayer++;
}
