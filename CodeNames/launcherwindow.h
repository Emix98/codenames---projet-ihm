#ifndef LAUNCHERWINDOW_H
#define LAUNCHERWINDOW_H

#include <QMainWindow>
#include <QMap>

#include "setupwindow.h"
#include "dialogoptions.h"
#include "dialogaide.h"
#include "dialoghostjoin.h"

namespace Ui {
class LauncherWindow;
}

class LauncherWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LauncherWindow(QWidget *parent = nullptr);
    ~LauncherWindow();

    static void initOptions();

    static QMap<QString, QString> options;
    static Network* network;

    void retranslateUI();

private slots:
    void on_local_button_clicked();

    void on_options_button_clicked();

    void on_help_button_clicked();

    void on_quit_button_clicked();

    void window_closed();

    void on_network_button_clicked();

private:
    Ui::LauncherWindow *ui;
    void changeEvent(QEvent *event);
};

#endif // LAUNCHERWINDOW_H
