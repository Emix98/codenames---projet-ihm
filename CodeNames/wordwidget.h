#ifndef WORDWIDGET_H
#define WORDWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QImage>
#include <QPixmap>

#include <QDebug>

#include "word.h"

namespace Ui {
class WordWidget;
}

class WordWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WordWidget(QWidget *parent = 0);
    ~WordWidget();
    int number;
    void displayWord(Word*wrd, bool hide);

signals:
    void clicked(int);

private:
    Ui::WordWidget *ui;

    QString getPictureForWord(Word* wrd, bool hide);
    void mouseReleaseEvent(QMouseEvent* ev);
};

#endif // WORDWIDGET_H
