#include "gword.h"

Word GWord::words[25];

bool GWord::initWords(QString filename, int nbBlue, int nbRed, int nbAss)
{
    QList<QString> l;
    QRandomGenerator rg = QRandomGenerator::securelySeeded();
    int rand = 0;
    QList<int> ids;
    //Ouverture du fichier
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;

    //Lecture ligne par ligne et remplissage de la QLists
    QTextStream in(&file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            l.append(line);
        }
    file.close();
    Team tmp;

    for(int i = 0;i<25;i++)
    {
        //Détermination de l'équipe du mot
        if(i<nbBlue)
            tmp = Team::Blue;
        else if(i<nbRed + nbBlue)
            tmp = Team::Red;
        else if(i< nbAss + nbRed + nbBlue)
            tmp = Team::Assassin;
        else
            tmp = Team::Witness;

        //Choix d'une ligne sans répétition
        while(ids.contains(rand))
            rand = rg.bounded(l.size());

        //Ajout du mot dans le tableau
        ids.append(rand);
        Word w = Word(l.at(rand),tmp);
        words[i] = w;
    }

    rand = rg.bounded(20);
    for (int i = -1;i<rand;i++)
        std::random_shuffle(&words[0],&words[25]);
    return true;
}

void GWord::cleanWords()
{
    Word ws[25];
    setWords(ws);
}

void GWord::setWords(Word* wds)
{
    for(int i = 0;i<25;i++)
    {
        words[i].copyFrom(wds[i]);
    }
}

Word* GWord::getWord(int id)
{
    if(id <0 || id >24)
        return nullptr;
    return &words[id];
}

Team GWord::discoverWord(int id)
{
    if(id<0 || id>24)
        return Team::NA;
    return words[id].discover();
}

void GWord::discoverRed()
{
    bool found = false;
    for(int i = 0;i<25 && !found;i++)
    {
        if(!words[i].isDiscovered() && words[i].getTeam() == Team::Red)
        {
            discoverWord(i);
            found = true;
        }
    }
}
