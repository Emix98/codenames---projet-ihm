#ifndef GAMEENGINE_H
#define GAMEENGINE_H
#include "gamestatus.h"
#include "gplayer.h"
#include "gword.h"
#include "globaldata.h"

class GameEngine
{
public:
    static void discover(int tuile);
    static void endOfTurn();
    static void endOfGame();
    static QString indice;
};

#endif // GAMEENGINE_H
