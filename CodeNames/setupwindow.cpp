#include "setupwindow.h"
#include "ui_setupwindow.h"

SetupWindow::SetupWindow(QWidget *parent, bool multiplayer) :
    QMainWindow(parent),
    ui(new Ui::SetupWindow),
    isMultiplayer(multiplayer)
{
    ui->setupUi(this);
    //On affiche le groupe de personnalisation seulement si la checkbox est selectionnée
    //Note : le changement dynamique est géré par un signal déclaré dans le designer
    this->ui->gpPersonnalisation->setVisible(this->ui->chkBxPerso->isChecked());

    //On connecte les signaux valueChanged des spinBox
    connect(ui->spNbEspB, SIGNAL(valueChanged(int)), this, SLOT(onSpinBoxValueChanged()));
    connect(ui->spNbEspR, SIGNAL(valueChanged(int)), this, SLOT(onSpinBoxValueChanged()));
    connect(ui->spNbAss, SIGNAL(valueChanged(int)), this, SLOT(onSpinBoxValueChanged()));

    //On définit les anciennes valeurs des spinboxs :
    oldSpValues[0] = ui->spNbEspB->value();
    oldSpValues[1] = ui->spNbEspR->value();
    oldSpValues[2] = ui->spNbAss ->value();

}

SetupWindow::~SetupWindow()
{
    delete ui;
}

void SetupWindow::closeEvent(QCloseEvent *ev)
{
    //En plus de faire le traitement normal, on envoi un signal de fermeture
    if(sendCloseEvent)
        emit windowClosing();
    QMainWindow::closeEvent(ev);
}

void SetupWindow::setupGame()
{
    int adb=0, adr=0;
    //On crée un gameStatus
    Team startTeam;
    if(this->ui->rbCoop->isChecked())
    {
        startTeam=Team::Blue;
    }
    else if(this->ui->cboStartTeam->currentIndex() == 0){
        startTeam = qrand() %2 == 0 ? Team::Red : Team::Blue;
    }

    else {
        startTeam = this->ui->cboStartTeam->currentIndex() == 1 ? Team::Red : Team::Blue;
    }

    //On ajoute l'agent double en fonction de l'équipe de départ et s'il y a au moins 1 témoin
    if((25-this->ui->spNbEspB->value() - this->ui->spNbEspR->value() - this->ui->spNbAss->value()) > 0 && startTeam == Team::Blue)
        adb=1;
    else if((25-this->ui->spNbEspB->value() - this->ui->spNbEspR->value() - this->ui->spNbAss->value()) > 0 && startTeam == Team::Red)
        adr=1;
    //On initialise les mots
    QString wordFile;

    if(GlobalData::options["lang"] == "English"){
        wordFile = QString(":/texts/en/wordsEN");
    } else {
        wordFile = QString(":/texts/fr/wordsFR");
    }

    GWord::initWords(wordFile, this->ui->spNbEspB->value()+adb, this->ui->spNbEspR->value()+adr, this->ui->spNbAss->value());

    GlobalData::status = new GameStatus(nullptr, this->ui->rbCompet->isChecked() ? Mod::Compet : Mod::Coop, this->ui->spNbEspB->value()+adb, this->ui->spNbEspR->value()+adr, startTeam, PType::SpyMaster, "", 0 );
}

void SetupWindow::on_btnStart_clicked()
{
    //On crée le réseau (faux ou serveur)
    if(this->isMultiplayer){
//        GlobalData::network = new ServerNetwork(20000);
        setupGame();
        //TODO: On passe sur le lobby

    } else {
        GlobalData::network = new FakeNetwork();
        setupGame();

        CodeNamesWindow* cNWin = new CodeNamesWindow(qobject_cast<QWidget*>(parent()));
        cNWin->show();

        this->sendCloseEvent = false;
        //On ferme et supprime cette fenètre
        //On cache juste cette fenètre
        this->hide();
        //this->deleteLater();
    }
}

void SetupWindow::onSpinBoxValueChanged(){
    //Les différentes spinbox

//    QSpinBox *spBoxes[3];
//    spBoxes[0] = ui->spNbEspB;
//    spBoxes[1] = ui->spNbEspR;
//    spBoxes[2] = ui->spNbAss;

    QSpinBox* changed= qobject_cast<QSpinBox*>(sender());

    //On vérifie que tout est cohérent
    int count = ui->spNbEspR->value() + ui->spNbEspB->value() + ui->spNbAss->value();
    if(count <= 25){
        //Le compte est bon
    } else {
        //Le compte n'est pas bon
        //On retourne à la valeur précédente
        if(changed == ui->spNbEspB){
            ui->spNbEspB->setValue(oldSpValues[0]);
        } else if(changed == ui->spNbEspR){
            ui->spNbEspR->setValue(oldSpValues[1]);
        } else if(changed == ui->spNbAss){
            ui->spNbAss->setValue(oldSpValues[2]);
        }
    }
    //On met les anciennes valeurs ç jour
    oldSpValues[0] = ui->spNbEspB->value();
    oldSpValues[1] = ui->spNbEspR->value();
    oldSpValues[2] = ui->spNbAss ->value();
    //Et on fini par recalculer le nombre de témoins
    this->ui->spTemoins->setValue(25 - (ui->spNbEspR->value() + ui->spNbEspB->value() + ui->spNbAss->value()));
}
