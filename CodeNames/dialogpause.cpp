#include "dialogpause.h"
#include "ui_dialogpause.h"

DialogPause::DialogPause(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPause)
{
    ui->setupUi(this);
}

DialogPause::~DialogPause()
{
    delete ui;
}
