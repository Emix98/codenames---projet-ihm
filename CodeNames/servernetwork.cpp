#include "servernetwork.h"

ServerNetwork::ServerNetwork(int port)
{
    this->port = port;
    this->server = new QTcpServer;
}

int ServerNetwork::getPort()
{
    return port;
}

QList<QString> *ServerNetwork::getAdresses()
{
    QList<QString>* addresses = new QList<QString>;

    foreach (QHostAddress address, QNetworkInterface::allAddresses()) {
        addresses->append(address.toString());
    }

    return addresses;
}

bool ServerNetwork::isReady()
{
    return this->ready;
}

ServerNetwork::~ServerNetwork()
{
    //En cas de destruction, on ferme et supprime toutes les Socket
    foreach (QTcpSocket* socket, this->clientSockets) {
        socket->abort();
        socket->deleteLater();
    }

    //Puis on supprime le serveur
    this->server->close();
    this->server->deleteLater();
}


void ServerNetwork::run()
{
    // On déplace le serveur dans ce thread, pour que les évenement s'éxécutent ici
    this->server->moveToThread(this);

    //OK, ça c'est dégeulasse, mais pour l'instant, ça va faire l'affaire
    //J'ai pas envie de tout réécrire
    //Permet au signaux ayant pour cible cet objet d'être lancés dans ce thread
    this->moveToThread(this);

    // On commence à écouter pour des connections entrantes
    if(!this->server->listen(QHostAddress::Any, (quint16)this->port)){
        //Si le serveur n'arrive pas à démarrer
        emit networkError(NetworkError::SERVER_CANT_START);

    } else {
        //On connecte le serveur à la gestion des nouveaux arrivants
        connect(this->server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
        emit networkReady();
    }

    //Et on finit par lancer la boucle évenementielle
    this->exec();
}


/*
 *
 * Implémentation des slots
 *
 */


void ServerNetwork::onDataReceived()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if(socket == nullptr){
        //TODO: gestion d'erreur si la socket est nulle
        return;
    }
    QDataStream stream(socket);

    quint32 packetSize;

    //On vérifie si le flux contient assez de données

    if(socket->bytesAvailable()< (int)sizeof(quint32)){
        //TODO: gestion de l'erreur
        return;
    }

    stream>>packetSize;


    if(socket->bytesAvailable() < packetSize){
        //TODO: gestion de l'erreur
        return;
    }

    NetworkMessage message;
    stream.readRawData((char*)&message, 1);

    QByteArray data = socket->read(packetSize-sizeof(NetworkMessage));
    Team tea;

    switch(message){
    //TODO: traitement des messages et redirection vers la bonne méthode/le bon signal
    case NetworkMessage::CHAT_MESSAGE:
        // On emet le signal pour le faire remonter sur l'interface
        emit onChatMessageReceived(QString::fromUtf8(data));
        //Puis on l'envoie à chaque clients
        QMetaObject::invokeMethod(this, SLOT(broadcastData(NetworkMessage,QByteArray)),Qt::QueuedConnection, Q_ARG(NetworkMessage, message), Q_ARG(QByteArray, data));
        break;
    case NetworkMessage::CHAT_MESSAGE_TEAM:
        tea = static_cast<Team>(data.at(0));
        data.remove(0,1);
        emit onChatMessageTeamReceived(QString::fromUtf8(data), tea);
        data.prepend(tea);
        QMetaObject::invokeMethod(this, SLOT(broadcastData(NetworkMessage,QByteArray)), Qt::QueuedConnection, Q_ARG(NetworkMessage, message), Q_ARG(QByteArray, data));
        break;
    default:
        emit networkError(NetworkError::UNKNOW_MESSAGE);
    }

}

void ServerNetwork::onNewConnection()
{
    QTcpSocket* nouveauClient = server->nextPendingConnection();
    this->clientSockets.append(nouveauClient);

    connect(nouveauClient, SIGNAL(readyRead()), this, SLOT(onDataReceived()));
    connect(nouveauClient, SIGNAL(disconnected()), this, SLOT(onDisconnection()));

}

void ServerNetwork::onDisconnection()
{
    //On récupère la socket qui a été déconnectée
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    // On vérfiie qu'lle existe
    if (socket==nullptr){
        //TODO: traiter déconnexion socket nulle
        return;
    }
    // On la retire de la liste des sockets
    this->clientSockets.removeOne(socket);
    //Puis on la marque pour suppression
    socket->deleteLater();
}

void ServerNetwork::onServerError(QAbstractSocket::SocketError error)
{
    NetworkError netError;

    switch(error){
    case QAbstractSocket::HostNotFoundError:
        netError = NetworkError::HOST_NOT_FOUND;
        break;
    case QAbstractSocket::ConnectionRefusedError:
        netError = NetworkError::CONNECTION_REFUSED;
        break;
    case QAbstractSocket::RemoteHostClosedError:
        netError = NetworkError::REMOTE_HOST_CLOSED_CONNECTION;
        break;
    default:
        netError = NetworkError::OTHER_SOCKET_ERROR;
        break;
    }

    emit networkError(netError);
}


QByteArray ServerNetwork::createPacket(NetworkMessage msg, QByteArray *data)
{
    QByteArray packet;
    QDataStream stream(&packet, QIODevice::WriteOnly);

    // On écrit les données, (voir ClientNetwork::sendData pour plus de détails)
    stream<<(quint32)0;
    stream<<(uchar)msg;
    stream.writeRawData(data->data(), data->size());
    stream.device()->seek(0);
    stream<<(quint32)((quint32)packet.size() - sizeof(quint32));

    return packet;
}


void ServerNetwork::broadcastData(NetworkMessage msg, QByteArray data)
{
    // On crée le paquet de données
    QByteArray packet = this->createPacket(msg,&data);
    // Et on l'envoie à tout les clients
    for(int i = 0; i < this->clientSockets.size(); i++){
        this->clientSockets[i]->write(packet);
    }
}

void ServerNetwork::sendDataToClient(QTcpSocket *client, NetworkMessage msg, QByteArray data)
{
    // On envoie le paquet à un client
    client->write(this->createPacket(msg,&data));
}

void ServerNetwork::sendChatMessage(QString msg)
{
    this->broadcastData(NetworkMessage::CHAT_MESSAGE, msg.toUtf8());
}

void ServerNetwork::sendTeamChatMessage(QString msg, Team tea)
{
    QByteArray data;
    data.append(tea);
    data.append(msg.toUtf8());
    this->broadcastData(NetworkMessage::CHAT_MESSAGE_TEAM, data);
}
