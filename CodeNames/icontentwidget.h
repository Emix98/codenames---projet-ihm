#ifndef ICONTENTWIDGET_H
#define ICONTENTWIDGET_H


#include <QWidget>
#include <QMainWindow>

class CodeNamesWindow;

class IContentWidget {

public:
    IContentWidget() {}
    virtual ~IContentWidget(){}

    virtual void connectSignals(CodeNamesWindow * window) =0;
    virtual void disconnectSignals(CodeNamesWindow * window) =0;
    virtual QWidget* associatedWidget() = 0;

};

#endif // ICONTENTWIDGET_H
